## API et représentation des données

l'API REST pour la ressource *commandes*. Celle-ci devrait répondre aux URI suivantes :

| Opération | URI                     | Action réalisée                               | Retour                                       |
|:----------|:------------------------|:----------------------------------------------|:---------------------------------------------|
| GET       | /commandes              | récupère l'ensemble des commandes             | 200 et un tableau des commandes              |
| GET       | /commandes/{id}         | récupère la commande d'identifiant id         | 200 et la commande                           |
|           |                         |                                               | 404 si id est inconnu                        |
| GET       | /commandes/{id}/name    | récupère le nom de la commande                | 200 et le nom de la commande                 |
|           |                         | d'identifiant id                              | 404 si id est inconnu                        |
| POST      | /commandes              | création d'une commande                       | 201 et l'URI de la ressource créée + représentation |
|           |                         |                                               | 400 si les informations ne sont pas correctes|
|           |                         |                                               | 409 si la commande existe déjà (même nom)    |
| DELETE    | /commandes/{id}         | destruction de la commande d'identifiant id   | 204 si l'opération à réussi                  |
|           |                         |                                               | 404 si id est inconnu                        |
| GET 	    | /commandes/pizza/{id}   | récupère les commande qui possède la pizza    | 200 et un tableau de commandes               |
| 	        | 	                      | d'identifiant id 	    		                    | 404 si id inconnu                            |
| GET 	    | /commandes/ingredients/{id} | récuperes les commande qui possède les ingrédients | 200 et un tableau de commande       |
| 	        | 	 			                | d'identifiants id	       	       	   	        | 404 si id inconnue                           |
| GET 	    | /commandes/pizza/ingredients/{id} | recuperes les commande qui possèdes les pizzas |                                   |
| 	        | 					              | qui possèdes les ingrédients d'identifiants id| 200 et un tableau de commande                |
| POST 	    | /commandes/{id}         | ajoute une pizza a la commande                | 201 et l'URI de la ressource créée + représentation         |
|           |                         |                                               | 400 si les informations ne sont pas correctes|
|           |                         |                                               | 409 si la commande existe déjà (même nom)    |
| DELETE    | /commandes/{id}/pizza/{id} | supprime la pizza d'identifiant id         | 204 si l'opération à réussie                 |
| 	        | 				                | de la commande d'identifiant id               | 404 si la pizza n'est pas présente           |


Un ingrédient comporte uniquement un identifiant et un nom.
Sa représentation JSON prendra donc la forme suivante :

{
  "id": 1,
  "pizza": ["Aquatique","montagnarde","forestiere"]
}
	
Lors de la création, l'identifiant n'est pas encore connu car il le sera fourni
par la bdd. il y aura aussi une représentation JSON qui comporte uniquement le nom :

{ "pizza": ["Aquatique","montagnarde","forestiére"] }