package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id INTEGER PRIMARY KEY, nom VARCHAR NOT NULL, prenom VARCHAR NOT NULL)")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandespizzas (commande INTEGER, pizza INTEGER, CONSTRAINT fk_commandes FOREIGN KEY(commande) REFERENCES commandes(id), CONSTRAINT fk_pizza FOREIGN KEY(pizza) REFERENCES pizzas(id))")
	void createAssociationTable();

	@Transaction
	default void createTable() {
		createCommandeTable();
		createAssociationTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS commandes")
	void dropCommandeTable();

	@SqlUpdate("DROP TABLE IF EXISTS commandespizzas")
	void dropAssociationTable();

	default void dropTable() {
		dropAssociationTable();
		dropCommandeTable();
	}

	@SqlUpdate("INSERT INTO commandes (nom, prenom) VALUES (:nom, :prenom)")
	@GetGeneratedKeys
	long insertCommande(String nom, String prenom);

	@SqlUpdate("INSERT INTO commandespizzas (commande, pizza) VALUES (:idcommande, :idpizza)")
	@GetGeneratedKeys
	long insertAssociations(long idcommande, long idpizza);

	default long insertNewCommande(String nom, String prenom, Long[] pizzas) {
		long idcommande = insertCommande(nom, prenom);
		for (long idpizza : pizzas) {
			insertAssociations(idcommande, idpizza);
		}
		return idcommande;
	}

	@SqlQuery("SELECT * FROM commandes")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAllCommandes();

	@Transaction
	default List<Commande> getAll() {
		List<Commande> commandes = getAllCommandes();
		return commandes;
	}

	@SqlQuery("SELECT * FROM commandes WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findCommandeById(long id);

	@SqlQuery("SELECT pizza FROM commandespizzas WHERE pizza = :id")
	Long[] findPizzasByCommandeId(long id);

	@Transaction
	default Commande findById(long id) {
		PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
		Commande commande = findCommandeById(id);
		ArrayList<Pizza> list = new ArrayList<Pizza>();
		for (Long l : findPizzasByCommandeId(id)) {
			list.add(pizzaDao.findById(l));
		}
		commande.setPizzas(list.toArray(new Pizza[list.size()]));
		return commande;
	}

	@SqlQuery("SELECT * FROM commandes WHERE nom = :name")
	@RegisterBeanMapper(Commande.class)
	Commande findByName(String name);

	@SqlUpdate("DELETE FROM commandes WHERE id = :id")
	void remove(long id);
}