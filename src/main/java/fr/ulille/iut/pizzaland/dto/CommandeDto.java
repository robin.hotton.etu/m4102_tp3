package fr.ulille.iut.pizzaland.dto;

import java.util.Arrays;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {
	private long id;
	private String nom;
	private String prenom;
	private Pizza[] pizzas;

	public CommandeDto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Pizza[] getPizzas() {
		return pizzas;
	}

	public void setPizzas(Pizza[] pizzas) {
		this.pizzas = pizzas;
	}

	@Override
	public String toString() {
		return "CommandeDto [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", pizzas=" + Arrays.toString(pizzas)
				+ "]";
	}
}