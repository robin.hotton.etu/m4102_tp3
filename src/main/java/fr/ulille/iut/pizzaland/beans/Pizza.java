package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.*;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredients;

	public Pizza() {
	}

	public Pizza(long id, String name) {
		this.id = id;
		this.name = name;
		this.ingredients = new ArrayList<Ingredient>();
	}

	public Pizza(long id, String name, List<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void addIngredients(Ingredient ingredient) {
		this.ingredients.add(ingredient);
	}

	public void removeIngredients(Ingredient ingredient) {
		this.ingredients.remove(ingredient);
	}

	public List<Ingredient> getIngredients() {
		return this.ingredients;
	}

	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setName(i.getName());
		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		return pizza;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String res = "Pizza [id=" + id + ", name=" + name + "] ";
		for (Ingredient i : ingredients) {
			res += i.toString();
		}
		return res;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		return dto;
	}

	public static Pizza fromIngredientCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		return pizza;
	}
}